module.exports = (app) => {
    const book = require('../controllers/book.controller.js');

    // Create a new Book
    app.post('/books', book.create);

    // Retrieve all Book
    app.get('/books', book.findAll);

    // Retrieve a single Book with bookId
    app.get('/books/:bookId', book.findOne);

    // Update a Book with bookId
    app.put('/books/:bookId', book.update);

    // Delete a Book with bookId
    app.delete('/books/:bookId', book.delete);
}